<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, and ABSPATH. You can find more information by visiting
 * {@link https://codex.wordpress.org/Editing_wp-config.php Editing wp-config.php}
 * Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

/**
 * Load environment config file
 */
if ( file_exists( dirname( __FILE__ ) . '/params.php' ) ) {
    require_once dirname( __FILE__ ) . '/params.php';
} else {
    exit( 'No config file found, please fix this.' );
}

/**
 * Load composer
 */
if ( file_exists( dirname( __DIR__ ). '/vendor/autoload.php' ) ) {
    require_once dirname( __DIR__ ) . '/vendor/autoload.php';
} else {
    exit( 'Run composer install.' );
}

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         ':- %ez!BDwlz}0mQf:?;iSSOmNg<VoxMQM4;vt9/cf*ROEI7@~&:8fYB:n92`dyF');
define('SECURE_AUTH_KEY',  '69b]h@iIZit|P{;~gt3qzV^~b1?0z.A{%np>aJECxyTIzs><jDA?[(z#RR?igOB>');
define('LOGGED_IN_KEY',    '_t4k{ZD#wYV`A%IM5&e&f`iz0?]c.4%MLep;SA6E.m{Tbk%F).=|3aOA*Uz-rEno');
define('NONCE_KEY',        't}-Z-w(UgJ{8qp3Aj&_x81S51/7<^>=Sx0F}a9Q1$|M}/:i8C]6*:MB;dkj9dGHQ');
define('AUTH_SALT',        'k5%E^@r={JKJ-_6K5&O<$Nx9(b6d%ch*z,{l,/:,rM9!}+&(5Qhk8e`j&_C1]Gge');
define('SECURE_AUTH_SALT', '@)?f`+G3eq^tVzc?c+}=B|pwEQ[EM39E%Tu`t0o=B[4j[YjhJ{_<y$pMu#{^xGU&');
define('LOGGED_IN_SALT',   'N~07m4DBYr:)Y~.H34#5|4&KNFA h=>RsRYL|lMWmi<rH&ai||j$KsV}WX>=`Q-h');
define('NONCE_SALT',       'Xeo6|QYk-=5gf)x_`bhW5=U4/Acs&_xNjHla;|$<R-E.o3 x/`Ve/uGk!4-;`[O)');
/**#@-*/

// disable installation/editing of plugins and themes. We use composer.
define( 'DISALLOW_FILE_MODS', true );

// disable WordPress cron. Run from task server.
define( 'DISABLE_WP_CRON', true );

// disable connections to WordPress.org
define( 'AUTOMATIC_UPDATER_DISABLED', true );

// Default lang
define( 'WPLANG', 'nl_NL' );

/** The name of the database for WordPress */
if ( ! defined( 'DB_NAME' ) ) {
    define( 'DB_NAME', DB_DATABASE );
}

/** MySQL database username */
if ( ! defined( 'DB_USER' ) ) {
    define( 'DB_USER', DB_USERNAME );
}

/** MySQL database password */
//if ( ! defined( 'DB_PASSWORD' ) ) {
//    define( 'DB_PASSWORD', DB_PASSWORD );
//}

/** MySQL hostname */
if ( ! defined( 'DB_HOST' ) ) {
    define( 'DB_HOST', DB_HOSTNAME );
}

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
if ( ! defined( 'WP_DEBUG' ) ) {
    define( 'WP_DEBUG', false );
}

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
    define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
